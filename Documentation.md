# Google UMP

## Remote Config Settings :

GdprType :  2

## Integration Steps

1) **"Install"** or **"Upload"** FG Google CMP plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Follow the instructions in the **"Install External Plugin"** section to import GoogleMobileAds SDK.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) To finish your integration, follow the **Account and Settings** section.

## Install External Plugin

After importing the FG GoogleUMP module from the Integration Manager window, you will find the last compatible version of GoogleMobileAds SDK in the _Assets > FunGames_Externals > GDPR_ folder. Double click on the .unitypackage file to install it.

**Note that the versions of included external SDKs are the latest tested and approved by our development team. We recommend using these versions to avoid any type of discrepencies. If, for any reason, you already have a different version of the plugin installed in your project that you wish to keep, please advise our dev team.**


## Account and Settings

To integrate Google UMP throught FunGames SDK you first need to register an application in the Google AdMob dashboard. Ask your Publisher to create your app on their dashboard and provide your App IDs.

If you use Admob as a network for Applovin, you can use same IDs and set it in the GoogleMobileAdsSettings (see their <a href="https://developers.google.com/admob/unity/quick-start?hl=fr#set_your_admob_app_id" target="_blank">documentation guide</a>).

<img src="_source/googleCMP.png" width="256" height="540" style="display: block; margin: 0 auto"/>


# TROUBLESHOOTING

## App crash at Start

If your app instantly crash when running it for the first time after integrating Google UMP, double check that you've set the proper App IDs in GoogleMobileAdsSettings. Make sure they also match with the ones set for Admob network in the Applovin Integration Manager.

## Build error with GameAnalytics

When building alongside with GA, you might face an issue of type :

**Assets\GameAnalytics\Plugins\Scripts\ILRD\AdMob\GAAdMobIntegration.cs(23,12): error CS1061:'BannerView' does not contain a definition for 'OnPaidEvent' and no accessible extension method 'OnPaidEvent' accepting a first argument of type 'BannerView' could be found (are you missing a using directive or an assembly reference?**

This is due to an incompatible version between GA and GoogleAdMob SDKs. You should be able to solve it by
updating GA to its latest version.

## Build error with ApplovinMax : "This feature requires ASM7"

When building alongside with Applovin Max, you might face an issue of type:

**"Execution failed for task ':launcher:dexBuilderRelease'. > java.lang.UnsupportedOperationException: This feature requires ASM7"**

This is due to a conflicting use of ASM version between both plugins. To solve it, you will need to enable _'android.enableDexingArtifactTransform'_ property. Problem is, this value is overriten in a post process build function from MaxSdk so you will need to change the following code, directly in MaxPostProcessBuildAndroid.cs script:

![](_source/googleUMP_troubleshooting_buildWithApplovin.png)

Once done, you should be able to build your app properly.
If issue persists, delete the Library/Bee folder from your project and try building again.

## Conflict with AudioMob

After importing GoogleMobileAds SDK in a Unity project where AudioMob is installed, you might see errors appearing in the console :

![](_source/googleUMP_troubleshooting_buildWithAudioMob.png)

The issue is about 'Countdown' class from AudioMob plugin confliting with GoogleMobileAds.Unity assembly 'Countdown'.

To solve it you will have to modify the 'CustomCountdown' class inside AudioMob plugin, and add the reference to AudioMob.Countdown as follow :

![](_source/googleUMP_troubleshooting_buildWithAudioMob1.png)